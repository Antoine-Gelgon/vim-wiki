%title Inkscape

=== Extensions ===
  * https://github.com/fablabnbg/inkscape-centerline-trace - > autotrace dans inkscape.

=== Ressources ===
  * [[http://how-to.wikia.com/wiki/How_to_use_Inkscape_in_commandline_mode/List_of_verbs |List of verbs]]
  * [[https://inkscape.org/en/gallery/%3Dextension/|list of extensions]]
