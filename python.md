%title Python

=== Ressources ===

  * http://dvillers.umons.ac.be/wiki/floss:python -> quelques références, trucs et astuces
  * http://sametmax.com/pipenv-solution-moderne-pour-remplacer-pip-et-virtualenv/ -> pipenv, solution moderne pour remplacer pip et virtualenv
  * http://effbot.org/zone/pythondoc-elemen£$ttree-ElementTree.htm#elementtree.ElementTree._ElementInterface.attrib-attribute -> The elementtree.ElementTree Module
