%title Pacman

== Memo & Ressources ==

Voir les dépendances inutilisées. `$ sudo pacman -Qdqt` * 

Suprimer package: `$ sudo pacman -Rcs [paquet]` * 

Lister tout les packages : `$ sudo pacman -Qe` * 

__*valable sur Pikaur & Yay__
