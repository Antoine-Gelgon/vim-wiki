== Memo ==
=== Border ===

==== border-image ====
[[https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Background_and_Borders/Border-image_generator]]
```
div {
 border:  80px solid transparent;
 padding: 15px;
 border-image: url(motif.svg) 33.7% round;
}
```

==== Selector ====
Selection en commençant par la fin. Ici selections des 8 dernière balise.

`p:nth-last-of-type(-n+8)`


== Ressources ==
  * [[http://www.cssplant.com/clip-path-generator |clip-path-generator]]
