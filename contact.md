%title Antoine Gelgon
%template index
== Contact ==
* Phone --> +32 488 85 84
* eMail --> antoine@luuse.io
* Telegram -> @AntoineGelgon 

== Social ==
* Mastodon -> AntoineGelgon@framapiaf.org
* Twitter -> [[@AntoineGelgon]]  

== Groups ==
=== Luuse ===
  * Web Site -> http://www.luuse.io
  * Git -> https://gitlab.com/Luuse 
=== OSP === 
  * Web Site -> http://osp.kitchen/ 
  * Git      -> https://gitlab.constantvzw.org/osp


