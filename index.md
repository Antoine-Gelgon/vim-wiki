%title rapide / informel
%template index
== antoine gelgon

[[contact]]

contenu sous licences -> cc by

== images ==
  * [[gallery.php?gal=luuser-type|luuser-type]]
== memo & ressources ==
  * [[vim]]
  * [[css]]
  * [[services-web]]
  * [[inkscape]] (extensions)
  * [[python]]
  * [[pacman]]
  * [[aur]]
  * [[unix]]
  * [[tmux]]
  * [[chiffrement]] 
  * [[svg]]
  * [[cpc]]
  * [[mysql]]

