%title SVG -> SCALABLE VECTOR GRAPHICS

== Memo ==

=== path ===

Attribut `d`

{{{
M = moveto
L = lineto
H = horizontal lineto
V = vertical lineto
C = curveto
S = smooth curveto
Q = quadratic Bézier curve
T = smooth quadratic Bézier curveto
A = elliptical Arc
Z = closepath
}}}

== Ressources ==
