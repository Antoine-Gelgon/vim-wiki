<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>rapide/informel</title>
  <link rel="stylesheet" type="text/css" href="css/reset.css" media="all">
  <link rel="stylesheet" type="text/css" href="css/style.css" media="all">
  <script src="js/jquery-2.2.4.min.js"></script>

</head>
<body>
  <header>
    <?php include('blog/index.html'); ?>
  </header>
  <div class="content">
  </div>
</body>
<script charset="utf-8">
  $(document).ready(function(){
    var loc = window.location.hash.substring(1);
    console.log(loc);
    if (loc == '') {

    }else{
      $('.content').load('blog/'+loc);
    }
    var li = $('.nav li a');
    li.each(function(){
      var liHref = $(this).attr('href');
      $(this).attr('data-href', liHref);
      $(this).removeAttr('href');
      console.log(liHref);
    })

    $('.nav li').click( function(){
      var href = $(this).children().attr('data-href');
      window.location.hash = href;
      $('.content').load('blog/'+href);
    });
  })
</script>
</html>
