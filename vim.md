== Memo ==
=== Plugin List ===
{{{
'VundleVim/Vundle.vim'
'gorodinskiy/vim-coloresque'
'vimwiki/vimwiki'
'gregsexton/matchtag'
'kien/rainbow_parentheses.vim'
'ervandew/supertab'
'ctrlpvim/ctrlp.vim'
'itchyny/lightline.vim'
'bling/vim-bufferline
'mbbill/undotree'
'nanotech/jellybeans.vim'
'sjbach/lusty'
'scrooloose/nerdcommenter'
'somini/vim-autoclose'
'tpope/vim-eunuch'
'tpope/vim-fugitive'
'tpope/vim-surround'
'vim-scripts/Align'
'MarcWeber/vim-addon-mw-utils'
'tomtom/tlib_vim'
'honza/vim-snippets'
'garbas/vim-snipmate'
'mhinz/vim-startify'
'mhinz/vim-signify'
'tomtom/tcomment_vim'
'mattn/emmet-vim'
'htacg/tidy-html5'
'yggdroot/indentline'
'chrisbra/unicode.vim'
'molok/vim-vombato-colorscheme'
'szorfein/darkest-space'
'jonathanfilip/vim-lucius'
'mustache/vim-mustache-handlebars'
'scrooloose/syntastic'
'majutsushi/tagbar'
 }}} 

=== Buffer ===

Pour copier de vim vers le press-copie -> `"+y`
{{{
 " Buffers, preferred over tabs now with bufferline.
  nnoremap <c-n> :bnext<CR>
  nnoremap <c-N> :bprevious<CR>
  nnoremap gd :bdelete<CR>
  nnoremap gf <C-^>
}}}

=== Vimwiki ===

Paramètres dans `.vimrc`
{{{
  " vimwiki command
  nmap <Leader>W <Plug>VimwikiIndex<CR>
  nmap <Leader>tt <Plug>VimwikiToggleListItem<CR>
  nmap <Leader>wf <Plug>VimwikiFollowLink<CR>
  nmap <Leader>wc <Plug>Vimwiki2HTML<CR>
}}}

=== Manipulation du contenu ===

Mettre tout la selection sur une ligne : `<C-j>`

==== Multiline edit ====

1. Put the cursor on the second word:
2. Hit <C-v> to enter visual-block mode and expand your selection toward the bottom:
3. Hit I"<Esc> to obtain:
4. Put the cursor on the last char of the third word:
5. Hit <C-v> to enter visual-block mode and expand your selection toward the bottom:
6. Hit A"<Esc> to obtain:



== Ressources ==

  * [[http://vimawesome.com/|Vimawesome]]- Bibliothèque de plugin Vim
  * [[http://vimcolors.com/ |vimColors]] - Bibliothèque de colorscheme
  * https://vimebook.com/fr - EBook vim -> La meilleure façon de rester productif en apprenant Vim
  * https://www.journalduhacker.net/s/nevdn7/question_jdh_les_meilleurs_plugins_vim - Liste JDH:  Les meilleurs plugins Vim du quotidien
