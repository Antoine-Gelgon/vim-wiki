%title Services Web

== Partage de fichier ==
  * https://transfer.sh/ -> Partager des fichiers en ligne de commande.
  * https://framadrop.org/ -> Une alternative framasoft à WeTransfer.
  * https://framadrop.org/ -> Une alternative framasoft à WeTransfer.

== Memo & prise de notes ==
  * https://joplin.cozic.net/ -> Joplin is a free, open source note taking and to-do application.
  * https://asciinema.org/ -> Record and share your terminal sessions, the right way. 
  * http://etherpad.org/ -> Etherpad is a highly customizable Open Source online editor providing collaborative editing in really real-time.

== Appels & Visioconférence ==
  * https://wire.com/en/ -> secure collaboration platform
