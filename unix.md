%title Unix

== Memo == 

=== USB bootable ===

Installer Linux avec sa clé USB.

Cibler la clé usb.

`sudo fdisk -f`

installer la clé.

`dd if=/path/to/iso of=/path/to/usb bs=512k && sync`

≃== Find ===

Chercher les fichiers contenant un occurence précise.
`find . -type f -print | xargs grep "text"`

== Ressources ==
https://explainshell.com/ -> write down a command-line to see the help text that matches each argument
